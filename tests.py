# -*- coding: utf-8; -*-

""" Unit tests for ‘vanilla.models.Item’. """

from __future__ import unicode_literals

import unittest

from django.conf import settings
from django.test import TestCase
from django.test.client import Client
from django_testscenarios.ubertest import TestCaseWithScenarios

from cam.tests import (
    PersonFactory,
    # Period, timestamps, setup_membership_product_fixture, make_product_config
)
from project.tests import UserFactory
from vanilla.models import Item, Member


class Item_get_absolute_url_TestCase(TestCase):
    """ Test cases for ‘Item.get_absolute_url’. """

    fixtures = ['test_items']

    def test_item_get_absolute_url(self):
        self.assertEquals(Item.objects.get(item_id=1).get_absolute_url(),
                          '/test-tab/')
        self.assertEquals(Item.objects.get(item_id=2).get_absolute_url(),
                          '/test-tab/test-section/')
        self.assertEquals(Item.objects.get(item_id=3).get_absolute_url(),
                          '/test-tab/test-section/3/')
        self.assertEquals(Item.objects.get(item_id=4).get_absolute_url(),
                          '/test-tab/test-section/4/')
        self.assertEquals(Item.objects.get(item_id=5).get_absolute_url(),
                          '/test-tab/test-section/5/')


class Item_extract_sort_num_TestCase(TestCaseWithScenarios):
    """ Test cases for ‘Item.extract_sort_num’. """

    scenarios = [
            ('with number', {
                'instance': Item(title="2. Hello"),
                'expected_value': (2, 'Hello'),
                }),
            ('sans number', {
                'instance': Item(title="Hello"),
                'expected_value': (None, 'Hello'),
                }),
            ('year only', {
                'instance': Item(title="2011"),
                'expected_value': (None, '2011'),
                }),
            ('title, leading year', {
                'instance': Item(title="2011 Hello"),
                'expected_value': (None, '2011 Hello'),
                }),
            ]

    def test_returns_expected_value(self):
        """ The return value should match the expected value. """
        result = self.instance.extract_sort_num()
        self.assertEqual(self.expected_value, result)


class MemberTestCase(TestCase):
    fixtures = ['member_sep_2011.json']

    def test_modifying_email_doesnt_change_username(self):
        """Modifying a staff email address shouldn't change the username.

        The member updated the username from the email address, until trac#260
        was addressed.

        """
        test_member = Member.objects.get(pk=-2)
        old_username = test_member.user.username
        test_member.email = "wallace@bicyclevictoria.com.au"
        test_member.save()
        self.assertEquals(old_username, test_member.user.username)


class MemberOnlyTestCase(TestCase):
    fixtures = ['test_items']

    def setUp(self):
        super(MemberOnlyTestCase, self).setUp()


        # self.member_user = UserFactory.create()

        self.user_nonmember = UserFactory.create()
        self.person_nonmember = PersonFactory.create(user=self.user_nonmember)
        self.client_nonmember = Client()
        self.client_nonmember.login(
            username=self.user_nonmember.username,
            password=self.user_nonmember.email)

        self.user_staff = UserFactory.create(is_staff=True, password='test')
        self.client_staff = Client()
        self.client_staff.login(
            username=self.user_staff.username,
            password='test')

        # setup_membership_product_fixture(self)
        # make_product_config([self.test_person], self.membership_product_base, None,
        #                     Period(timestamps['last_week'], timestamps['next_week']))
        self.item = Item.objects.get(pk=2)

        self.client_anon = Client()

        # self.client_auth = Client()
        # self.client_auth.login(
        #     username=self.test_person.user.username,
        #     password=self.test_person.user.email)


    def test_anon_request_for_public_page_succeeds(self):
        self.item.member_only = False
        self.item.save()
        response = self.client_anon.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_anon_request_for_member_page_redirects_login(self):
        self.item.member_only = True
        self.item.save()
        response = self.client_anon.get(self.item.get_absolute_url())
        self.assertRedirects(response,
                             '%s?next=%s' % (settings.LOGIN_URL,
                                             self.item.get_absolute_url()),
                             302, 200)

    @unittest.skip("TODO: Fix this.")
    def test_member_served_public_page(self):
        # Logged in member accessing public page
        self.item.member_only = False
        self.item.save()
        response = self.client_auth.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    @unittest.skip("TODO: Fix this.")
    def test_member_served_member_page(self):
        # Logged in member accessing member-only page
        self.item.member_only = True
        self.item.save()
        response = self.client_auth.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_non_member_request_for_public_page_succeeds(self):
        """Logged in non-member accessing public page."""
        self.item.member_only = False
        self.item.save()
        response = self.client_nonmember.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_non_member_request_for_member_page_redirect_to_login(self):
        """Logged in non-member accessing member-only page."""
        self.item.member_only = True
        self.item.save()
        response = self.client_nonmember.get(self.item.get_absolute_url())
        self.assertRedirects(response, '/accounts/login/?next=/test-tab/test-section/')

    def test_staff_request_for_public_page_succeeds(self):
        """Logged in staff accessing public page."""
        self.item.member_only = False
        self.item.save()
        response = self.client_staff.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_staff_request_for_member_page_succeeds(self):
        """Logged in staff accessing member-only page."""
        self.item.member_only = True
        self.item.save()
        response = self.client_staff.get(self.item.get_absolute_url())
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
