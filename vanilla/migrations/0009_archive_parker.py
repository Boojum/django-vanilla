# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import itertools

from django.db import migrations

def archive_parker(apps, schema_editor):
    ITEM_ID_PARKER = 50

    Item = apps.get_model('vanilla', 'Item')
    parker_tabs = Item.objects.filter(item_id=ITEM_ID_PARKER)
    parker_sections = Item.objects.filter(relate__in=parker_tabs)
    parker_sub_sections = Item.objects.filter(relate__in=parker_sections)
    parker_topics = Item.objects.filter(relate__in=parker_sub_sections)
    parker_pages = Item.objects.filter(relate__in=parker_topics)

    parker_items = itertools.chain(
        parker_tabs, parker_sections, parker_sub_sections, parker_topics,
        parker_pages)

    for item in parker_items:
        item.archived = True
        item.save()


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0008_auto_20150904_1515'),
    ]

    operations = [
        migrations.RunPython(archive_parker)
    ]
