# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0003_auto_20150421_2334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='search_text',
        ),
        migrations.RemoveField(
            model_name='item',
            name='search_text_pending',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_level',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_level_pending',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_owner',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_owner_pending',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_search',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_search_pending',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_send_friend',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_send_friend_pending',
        ),
    ]
