# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0009_archive_parker'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='seo_description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='item',
            name='seo_title',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
