# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations

def create_home_page(apps, schema_editor):
    ItemType = apps.get_model('vanilla', 'ItemType')
    ItemType.objects.bulk_create([
        ItemType(item_type_id=1, title='Tab'),
        ItemType(item_type_id=2, title='Section'),
        ItemType(item_type_id=3, title='Sub-section'),
        ItemType(item_type_id=4, title='Topic'),
        ItemType(item_type_id=5, title='Page')])

    Item = apps.get_model('vanilla', 'Item')
    # Create home page.
    Item.objects.create(
        type_id=1,
        status_id=1,
        edit_mode_id=1,
        title='General',
        summary='Sample summary',
        content='Sample home page content',
        title_pending='General',
        summary_pending='Sample summary',
        content_pending='Sample home page content',
    )
    # Create Parker intranet home page.
    Item.objects.create(
        item_id=50,
        type_id=1,
        status_id=1,
        edit_mode_id=1,
        title='Parker',
        summary='Sample summary',
        content='Sample intranet content',
        title_pending='Parker',
        summary_pending='Sample summary',
        content_pending='Sample intranet content',
    )

class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0005_auto_20150430_0005'),
    ]

    operations = [
        migrations.RunPython(create_home_page)
    ]
