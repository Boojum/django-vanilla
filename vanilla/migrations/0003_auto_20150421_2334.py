# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0002_auto_20150421_1026'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='member',
            name='address1',
        ),
        migrations.RemoveField(
            model_name='member',
            name='address2',
        ),
        migrations.RemoveField(
            model_name='member',
            name='country',
        ),
        migrations.RemoveField(
            model_name='member',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='member',
            name='gender',
        ),
        migrations.RemoveField(
            model_name='member',
            name='organisation',
        ),
        migrations.RemoveField(
            model_name='member',
            name='phone_mobile',
        ),
        migrations.RemoveField(
            model_name='member',
            name='phone_work',
        ),
        migrations.RemoveField(
            model_name='member',
            name='police_date',
        ),
        migrations.RemoveField(
            model_name='member',
            name='postcode',
        ),
        migrations.RemoveField(
            model_name='member',
            name='ref_date',
        ),
        migrations.RemoveField(
            model_name='member',
            name='security_notes',
        ),
        migrations.RemoveField(
            model_name='member',
            name='state',
        ),
        migrations.RemoveField(
            model_name='member',
            name='suburb',
        ),
        migrations.RemoveField(
            model_name='member',
            name='surname',
        ),
    ]
