from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('item_id', models.AutoField(serialize=False, primary_key=True, db_column='ItemId')),
                ('archived', models.BooleanField(default=False, db_column='ItemArchive')),
                ('status_id', models.IntegerField(default=1, db_column='ItemStatusId', choices=[(1, 'live'), (2, 'modified'), (3, 'new')])),
                ('edit_mode_id', models.IntegerField(db_column='ItemEditModeId', choices=[(1, 'approved'), (2, 'for approval'), (3, 'draft')])),
                ('permission', models.IntegerField(null=True, editable=False, db_column='ItemPermission', blank=True)),
                ('member_only', models.BooleanField(default=False)),
                ('menu_hide', models.BooleanField(default=False, db_column='ItemMenuHide')),
                ('date', models.DateField(auto_now=True, null=True, db_column='ItemDate')),
                ('publish_date', models.DateField(null=True, db_column='ItemPublishDate', blank=True)),
                ('review_date', models.DateField(null=True, db_column='ItemReviewDate', blank=True)),
                ('title', models.CharField(max_length=255, db_column='ItemTitle', blank=True)),
                ('summary', models.TextField(db_column='ItemText1', blank=True)),
                ('illustration', models.ImageField(upload_to='vanilla_item', null=True, verbose_name='Page image', blank=True)),
                ('content', models.TextField(db_column='ItemText2', blank=True)),
                ('search_text', models.TextField(db_column='ItemText3', blank=True)),
                ('show_sub_content_b', models.BooleanField(default=False, db_column='ItemPanelShow')),
                ('sub_content_b', models.TextField(db_column='ItemPanelText1', blank=True)),
                ('show_owner', models.BooleanField(default=False, db_column='ItemInfoPanelShow1')),
                ('show_send_friend', models.BooleanField(default=False, db_column='ItemInfoPanelShow3')),
                ('show_search', models.BooleanField(default=False, verbose_name='Show search terms', db_column='ItemInfoPanelShow4')),
                ('show_level', models.BooleanField(default=False, db_column='ItemInfoPanelShow5')),
                ('show_send_photo', models.BooleanField(default=False)),
                ('title_pending', models.CharField(max_length=255, db_column='ItemTitlePending', blank=True)),
                ('summary_pending', models.TextField(db_column='ItemText1Pending', blank=True)),
                ('content_pending', models.TextField(db_column='ItemText2Pending', blank=True)),
                ('search_text_pending', models.TextField(db_column='ItemText3Pending', blank=True)),
                ('show_sub_content_b_pending', models.BooleanField(default=False, db_column='ItemPanelShowPending')),
                ('sub_content_b_pending', models.TextField(db_column='ItemPanelText1Pending', blank=True)),
                ('show_owner_pending', models.BooleanField(default=False, db_column='ItemInfoPanelShow1Pending')),
                ('show_send_friend_pending', models.BooleanField(default=False, db_column='ItemInfoPanelShow3Pending')),
                ('show_search_pending', models.BooleanField(default=False, db_column='ItemInfoPanelShow4Pending')),
                ('show_level_pending', models.BooleanField(default=False, db_column='ItemInfoPanelShow5Pending')),
                ('show_send_photo_pending', models.BooleanField(default=False)),
                ('allow_content_js', models.BooleanField(default=False, verbose_name='Allow Javascript', db_column='ItemAllowContentJS')),
                ('change_summary', models.TextField(blank=True)),
                ('change_summary_count', models.IntegerField(default=0)),
                ('redirect_to', models.CharField(help_text='Instead of displaying this page, redirect somewhere else.', max_length=50, blank=True)),
                ('relate', models.ForeignKey(on_delete=models.CASCADE, db_column='ItemRelateId', blank=True, to='vanilla.Item', null=True, verbose_name='Parent item')),
            ],
            options={
                'db_table': 'item',
                'permissions': (('add_edit_item', 'Can add and edit website pages'), ('publish_item', 'Can publish changes to website pages')),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemEditMode',
            fields=[
                ('item_edit_mode_id', models.IntegerField(serialize=False, primary_key=True, db_column='ItemEditModeId')),
                ('title', models.CharField(max_length=100, db_column='ItemEditModeTitle', blank=True)),
            ],
            options={
                'db_table': 'itemeditmode',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemStatus',
            fields=[
                ('item_status_id', models.IntegerField(serialize=False, primary_key=True, db_column='ItemStatusId')),
                ('title', models.CharField(max_length=100, db_column='ItemStatusTitle', blank=True)),
            ],
            options={
                'db_table': 'itemstatus',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemType',
            fields=[
                ('item_type_id', models.IntegerField(serialize=False, primary_key=True, db_column='ItemTypeId')),
                ('title', models.CharField(max_length=255, db_column='ItemTypeTitle', blank=True)),
            ],
            options={
                'db_table': 'itemtype',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('user', models.ForeignKey(on_delete=models.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('first_name', models.CharField(max_length=255, db_column='MemberFirstName')),
                ('surname', models.CharField(max_length=255, db_column='MemberSurname')),
                ('phone_work', models.CharField(max_length=50, db_column='MemberPhoneWork', blank=True)),
                ('phone_mobile', models.CharField(max_length=50, db_column='MemberMobile', blank=True)),
                ('address1', models.CharField(max_length=255, db_column='MemberAddress1', blank=True)),
                ('address2', models.CharField(max_length=255, db_column='MemberAddress2', blank=True)),
                ('suburb', models.CharField(max_length=255, db_column='MemberSuburb', blank=True)),
                ('state', models.CharField(max_length=100, db_column='MemberState', blank=True)),
                ('postcode', models.CharField(max_length=20, db_column='MemberPostcode', blank=True)),
                ('country', models.CharField(default='Australia', max_length=255, db_column='MemberCountry', blank=True)),
                ('organisation', models.CharField(max_length=255, db_column='MemberOrganisation', blank=True)),
                ('role', models.CharField(max_length=255, db_column='MemberRole', blank=True)),
                ('gender', models.CharField(blank=True, max_length=50, db_column='MemberGender', choices=[('Male', 'Male'), ('Female', 'Female')])),
                ('permission', models.IntegerField(null=True, db_column='MemberPermission', blank=True)),
                ('ref_date', models.DateField(null=True, verbose_name='Ref. check date', db_column='MemberRefDate', blank=True)),
                ('police_date', models.DateField(null=True, verbose_name='Police check date', db_column='MemberPoliceDate', blank=True)),
                ('security_notes', models.TextField(db_column='MemberSecurityNotes', blank=True)),
            ],
            options={
                'db_table': 'member',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('photo_id', models.AutoField(serialize=False, primary_key=True)),
                ('time', models.DateTimeField(auto_now_add=True, null=True)),
                ('status', models.CharField(default='new', max_length=10, choices=[('new', 'pending approval'), ('active', 'published'), ('archived', 'archived'), ('deleted', 'deleted')])),
                ('image', models.ImageField(help_text='In JPG format and less than 5 MB', upload_to='photo')),
                ('caption', models.CharField(default='', help_text='Up to 250 characters', max_length=255, blank=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=75)),
                ('item', models.ForeignKey(on_delete=models.CASCADE, to='vanilla.Item', null=True)),
            ],
            options={
                'db_table': 'photo',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='item',
            name='type',
            field=models.ForeignKey(on_delete=models.CASCADE, to='vanilla.ItemType', db_column='ItemTypeId'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='user',
            field=models.ForeignKey(on_delete=models.CASCADE, verbose_name='Owner', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
