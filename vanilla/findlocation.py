import pygeoip

NSW_POSTAL_CODE_RANGES = set([range(1000, 1999), range(2000, 2599), range(2619, 2899), range(2921, 2999)])
NSW_REDIRECT_IDS = [4453] # item_id for Membership pages

def record_by_addr(ip, geoip_database):
    ''' Returns None for localhost IP address (127.0.0.1)
    '''
    try:
        gi = pygeoip.GeoIP(geoip_database)
        return gi.record_by_addr(ip)
    except FileNotFoundError:
        return None
    except OSError as e:
        logger.info(str(e) + '; Log IP address: %s', ip)
        return None
    except: # pylint: disable=bare-except
        logger.info('Unexpected error; Log IP address: %s', ip)
        return None

def string_to_int(s):
    try:
        return int(s)
    except ValueError:
        return None

def in_NSW(ip, geoip_database):
    record = None
    if ip:
        record = record_by_addr(ip, geoip_database)
    if record and ('postal_code' in record) and record['postal_code']:
        postal_code = string_to_int(record['postal_code'])
        if postal_code:
            for postal_code_range in NSW_POSTAL_CODE_RANGES:
                if postal_code in postal_code_range:
                    return True
    return False


