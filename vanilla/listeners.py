import logging

from django.conf import settings

logger = logging.getLogger(__name__)

def hide_menu_in_news(sender, instance, **kwargs):
    """Force news items not to be shown in menus.

    This approach of using signals is to avoid creating a special page type for
    news items, which we may have to do at some point.

    """
    if (instance.relate_id == settings.VANILLA_CONFIG['NEWS_PARENT_ITEM']
        and not instance.menu_hide):
        instance.menu_hide = True
        logger.debug("Page \"{}\" updated to not be shown in menus.".format(
            instance))
