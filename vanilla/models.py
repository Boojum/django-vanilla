"""Models for the Vanilla simple CMS app."""

# TODO: Tab content (ie. Home) should live on the tab Item model.
# Currently content lives on the section beneath and the menus are tweaked to
# make it look like the content lives on the tab.

from datetime import datetime
import logging
import re

import bleach
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models, transaction
from django.db.models import Q
from django.db.models.signals import pre_save
from django.template.defaultfilters import slugify
from PIL import Image, ImageOps

from .listeners import hide_menu_in_news
import record_lock.models

ITEM_ID_ROOT = 1
ITEM_ID_HOME = 2

TYPE_ID_TAB = 1
TYPE_ID_SECTION = 2
TYPE_ID_SUBSECTION = 3
TYPE_ID_TOPIC = 4
TYPE_ID_PAGE = 5

STATUS_ID_LIVE = 1
STATUS_ID_PENDING = 2
STATUS_ID_NEW = 3

EDIT_MODE_APPROVE = 1
EDIT_MODE_READY = 2
EDIT_MODE_DRAFT = 3

BLINK_SHOP_BLINK = 16
BLINK_MY_BENEFITS = 43727
BLINK_BIKE_SERVICE = 30

DAO_LOCK_EXPIRY = 900 # 15 mins

# Dictionary settings inspired by:
# PyCon 2011: Pluggable Django Patterns
# http://blip.tv/pycon-us-videos-2009-2010-2011/pycon-2011-pluggable-django-patterns-4900929

# Default settings: You may override these in a project settings dictionary
# called VANILLA_CONFIG.
config = {
    'NEWS_PARENT_ITEM': None,
    'ITEM_ILLUSTRATION_SIZES': {
        'landscape': (504, 281),
        'wide-landscape': (726, 281)},
    'ALLOWED_HTML_TAGS': [
        'a', 'audio', 'button', 'abbr', 'area', 'article', 'blockquote', 'br',
        'cite', 'code', 'div', 'dd', 'dl', 'dt', 'em', 'h1', 'h2', 'h3', 'h4',
        'h5', 'h6', 'i', 'iframe', 'img', 'li', 'map', 'object', 'ol', 'p',
        'pre', 'q', 'span', 'strong', 'table', 'tbody', 'td', 'tfoot', 'th',
        'thead', 'tr', 'ul', 'video'],
    'ALLOWED_HTML_ATTRIBUTES': [
        'align', 'alt', 'autobuffer', 'class', 'controls', 'coords',
        'data-ride', 'data-slide', 'data-slide-to', 'data-target', 'height',
        'href', 'id', 'name', 'shape', 'src', 'style', 'target', 'title',
        'usemap', 'width', 'border', 'cellspacing', 'cellpadding', 'colspan',
        'rowspan'],
    'ALLOWED_HTML_STYLES': [
        'background', 'color', 'float', 'font-size', 'height', 'width',
        'text-align', 'vertical-align'],
}
config.update(getattr(settings, 'VANILLA_CONFIG', {}))

logger = logging.getLogger(__name__)

class MemberManager(models.Manager):
    def inactive(self):
        return Member.objects.filter(user__is_active=False).order_by('user__first_name', 'user__last_name')


    def live(self):
        return Member.objects.filter(user__is_active=True).order_by('user__first_name', 'user__last_name')


class Member(models.Model):
    """Member will hopefully be replace by Django Auth eventually."""

    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    role = models.CharField(max_length=255, db_column='MemberRole', blank=True)

    objects = MemberManager()

    class Meta:
        db_table = 'member'

    def save(self, *args, **kwargs):
        """Save duplicated fields to User."""

        # For a new staff member we need to create a User
        if self.user_id is None:
            self.user = User.objects.create(is_staff=True)
            logger.debug('Created new staff User #{id}.'.format(
                id=self.user.id))

        super().save(*args, **kwargs)

    def __str__(self):
        return self.user.get_full_name()


class ItemManager(record_lock.models.LockingManager):
    def search(self, phrase):
        """Return a list of search results in order of relevance.

         - return no results if phrase is less than three letters
         - return no results if phrase is "and", "but" or "the"
         - remove characters ".", ";" and "," from search string
         - don't return archived pages
         - in order of priority, search:
           - title
           - summary
           - content
         - don't list items twice

        """
        # TODO: This really isn't a scalable way to approach searching, since it
        # requires a full scan. We don't actually have all that many pages
        # though, so we may get away with it for a while.

        MIN_PHRASE_LENGTH = 3
        MAX_RESULTS = 40
        phrase = '%' + phrase.lower().replace('.', '').replace(';', '').replace(',', '') + '%'
        if (len(phrase) < MIN_PHRASE_LENGTH
            or phrase in ('and', 'but', 'the')):
            return None

        # Had to alias ItemId to item_id in Django 1.8 to prevent InvalidQuery
        # error. May have stumbled on a regression bug in 1.8 where it no longer
        # respects the db_column on the primary key.
        query = ("""
            SELECT ItemId as item_id, results.* FROM (
                SELECT *, 1 AS priority FROM item
                WHERE itemtitle LIKE %s AND itemarchive = 0
            UNION DISTINCT
                SELECT *, 2 AS priority FROM item
                WHERE itemtext1 LIKE %s AND itemarchive = 0
            UNION DISTINCT
                SELECT *, 3 AS priority FROM item
                WHERE itemtext2 LIKE %s AND itemarchive = 0)
            AS results
            GROUP BY itemid
            ORDER BY priority ASC
            LIMIT %s""")

        placeholder_values = ([phrase, phrase, phrase, MAX_RESULTS])
        results = self.model.objects.raw(query, placeholder_values)

        # Knock out any results that have archived parents. This is inefficient,
        # but required to ensure that archived status is propagated down.
        results = [item for item in results if not item.has_archived_ancestors()]

        return results

    def for_approval(self):
        return self.model.objects.filter(
            archived=False,
            status_id__in=(STATUS_ID_PENDING, STATUS_ID_NEW),
            edit_mode_id=EDIT_MODE_READY,
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('-date')

    def past_review_date(self):
        return self.model.objects.filter(
            archived=False,
            review_date__lte=datetime.today(),
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('-review_date')

    def draft(self):
        return self.model.objects.filter(
            archived=False,
            status_id__in=(STATUS_ID_PENDING, STATUS_ID_NEW),
            edit_mode_id=EDIT_MODE_DRAFT,
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('-date')

    def before_publish_date(self):
        return self.model.objects.filter(
            archived=False,
            publish_date__gte=datetime.today(),
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('publish_date')

    def live(self):
        return self.model.objects.exclude(status_id=STATUS_ID_NEW).filter(
            archived=False,
            publish_date__lte=datetime.today(),
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('-date')

    def archived(self):
        return self.model.objects.filter(
            archived=True,
            type_id__in=(TYPE_ID_TOPIC, TYPE_ID_PAGE)).order_by('-date')

    def news(self):
        """Return a queryset of all news Items."""
        PARENT_ITEM = settings.VANILLA_CONFIG['NEWS_PARENT_ITEM']
        if PARENT_ITEM is not None:
            return self.model.objects.exclude(status_id=STATUS_ID_NEW).filter(
                archived=False,
                publish_date__lte=datetime.today(),
                relate=PARENT_ITEM).order_by('-publish_date', 'title')
        else:
            return []


class Item(record_lock.models.LockingModel):
    """A page in the editable website.

    TODO: The current approach of hard-coding levels to each item in
    addition to maintaining a link to the parent item causes
    complications. Recommend we think out a more general approach, avoiding
    concepts of "tab", "section", "subsection", "topic", "subtopic" and
    "page" for everything except interface styling.

    Each item has two copies of a page, one to display to the public and
    one to work on (called pending). I don't really understand how
    status_id and edit_mode_id work, but here are the states:

                                                status_id	edit_mode_id
    No live copy, pending copy in draft		3		3
    No live copy, pending copy for approval	3		2
    Live copy, pending copy is same as live	1		1
    Live copy, pending copy in draft		2		3
    Live copy, pending copy for approval	2		2

    We also have the archived field.

    From this, I gather that a page should be displayed to the public if
    status_id is 1 or 2 and archived is false. See Item.is_published()

    Since this website viewer only displays the live copy, we don't need to
    worry about the other cases.
    """

    item_id = models.AutoField(primary_key=True, db_column='ItemId')
    user = models.ForeignKey(User, verbose_name='Owner', null=True, blank=True, on_delete=models.CASCADE)
    type = models.ForeignKey('ItemType', db_column='ItemTypeId', on_delete=models.CASCADE)
    relate = models.ForeignKey(
        'Item', verbose_name='Parent item', db_column='ItemRelateId', null=True,
        blank=True, on_delete=models.CASCADE)
    archived = models.BooleanField(db_column='ItemArchive', default=False)
    status_id = models.IntegerField(
        db_column='ItemStatusId', default=1,
        choices=((1, 'live'),
                 (2, 'modified'),
                 (3, 'new')))
    edit_mode_id = models.IntegerField(
        db_column='ItemEditModeId', default=3,
        choices=((1, 'approved'),
                 (2, 'for approval'),
                 (3, 'draft')))
    member_only = models.BooleanField(default=False)
    # Don't show in menu
    menu_hide = models.BooleanField(db_column='ItemMenuHide', default=False)
    date = models.DateField(null=True, db_column='ItemDate', blank=True, auto_now=True)
    publish_date = models.DateField(null=True, db_column='ItemPublishDate', blank=True)
    review_date = models.DateField(null=True, db_column='ItemReviewDate', blank=True)
    title = models.CharField(max_length=255, db_column='ItemTitle', blank=True)
    seo_title = models.CharField(
        max_length=255, blank=True, help_text='Page title seen by search engines')
    seo_description = models.TextField(
        blank=True, help_text='Page description seen by search engines')
    summary = models.TextField(db_column='ItemText1', blank=True)
    illustration = models.ImageField('Page image', upload_to='vanilla_item', null=True, blank=True)
    content = models.TextField(db_column='ItemText2', blank=True)
    show_sub_content_b = models.BooleanField(db_column='ItemPanelShow', default=False)
    sub_content_b = models.TextField(db_column='ItemPanelText1', blank=True)
    # "pending" fields are used for the draft version of a page
    title_pending = models.CharField(
        max_length=255, db_column='ItemTitlePending', blank=True)
    summary_pending = models.TextField(db_column='ItemText1Pending', blank=True)
    content_pending = models.TextField(db_column='ItemText2Pending', blank=True)
    show_sub_content_b_pending = models.BooleanField(db_column='ItemPanelShowPending', default=False)
    sub_content_b_pending = models.TextField(db_column='ItemPanelText1Pending', blank=True)
    # When false, automatically strip Javascript from content before displaying
    allow_content_js = models.BooleanField(
        'Allow unsafe HTML', db_column='ItemAllowContentJS', default=False)
    change_summary = models.TextField(blank=True)
    change_summary_count = models.IntegerField(default=0)
    redirect_to = models.CharField(
        # Useful for fake menu items.
        max_length=50, blank=True,
        help_text='Instead of displaying this page, redirect somewhere else.')

    objects = ItemManager()

    class Meta:
        db_table = 'item'
        # Note that since vanilla is under migrations, you'll need to run
        # `syncdb --all` to create the permissions.
        permissions = (
            ('add_edit_item', 'Can add and edit website pages'),
            ('publish_item', 'Can publish changes to website pages'))

    def __str__(self):
        return '{} ({})'.format(self.extract_sort_num()[1], self.pk)

    def save(self, *args, **kwargs):
        """Save the model instance, resizing the illustration if necessary."""

        sizes = config['ITEM_ILLUSTRATION_SIZES']

        super().save(*args, **kwargs)
        if self.illustration:
            try:
                # This may not make the right decision if the author is saving a
                # draft and has hidden/shown the panel differently to the live
                # version. Most of the time, that's ok though.
                if self.show_sub_content_b:
                    width, height = sizes['landscape']
                else:
                    width, height = sizes['wide-landscape']
                if self.illustration.height > height:
                    # This has some edge-cases - a really wide/high image that's has
                    # the other dimension already below our limits won't be
                    # modified. Turns out the in-browser resampling up via CSS
                    # "background-size: cover" looks better than that from PIL doing
                    # the same thing.
                    fit_image_field(self.illustration, width, height)
            except FileNotFoundError as err:
                logger.warning(
                    'Clearing illustration for {item}: {err}'.format(
                        item=self, err=err))
                self.illustration = None
                super().save(*args, **kwargs)


    def get_page_contents(self, staff=False):
        """Return either the draft contents or the live contents based on some
        conditions.

        Assumes that access has already been granted to view this page.

        """
        if self.edit_mode_id == STATUS_ID_LIVE or not staff:
            contents = {
                'title': self.extract_sort_num()[1],
                'summary': self.summary,
                'content': self.content,
                'sub_content_b': self.show_sub_content_b,
                'sub_content_b': self.sub_content_b,
                }
        else:
            contents = {
                'title': self.extract_sort_num(pending=True)[1],
                'summary': self.summary_pending,
                'content': self.content_pending,
                'sub_content_b': self.show_sub_content_b_pending,
                'sub_content_b': self.sub_content_b_pending,
                }
        if staff and self.status_text() != 'live':
            contents['title'] += " ({})".format(self.status_text())
        return contents

    def get_ancestors(self):
        """Return the tab, section, subsection, topic and page ancestor items.

        Each item in the tree will have either one or zero ancestors at a
        given level.

        This tree of items has both hard-coded levels and parent (relate)
        links. Parents and children items can be on the same level, or
        different levels (eg. parent itemid 1 and child itemid 10 both have
        typeid = 1). The top level items have relate_ids of 0, not
        NULL.
        """
        tab = None
        section = None
        subsection = None
        topic = None

        # Climb up the item tree till we get to a child of the root item
       # (or to the top), taking note of a section item we find.
        curr = self
        while curr.relate_id:
            if curr.type_id == TYPE_ID_TOPIC:
                topic = curr
            elif curr.type_id == TYPE_ID_SUBSECTION:
                subsection = curr
            elif curr.type_id == TYPE_ID_SECTION:
                section = curr
            elif curr.type_id == TYPE_ID_TAB:
                tab = curr
            curr = curr.relate
        if tab is None:
            tab = curr
        return (tab, section, subsection, topic)

    def get_children(self):
        return list(Item.objects.filter(relate=self))

    def get_absolute_url(self):
        return self.__get_legacy_absolute_url()

    def __get_legacy_absolute_url(self):
        """Return URL of this item."""
        (tab, section, _, _) = self.get_ancestors()

        # Put the URL together using the tab slug, section slug and the
        # itemid. Not all are required.
        tab_slug = slugify((tab.extract_sort_num()[1]).replace('&', 'and'))
        if section is not None:
            section_slug = slugify((section.extract_sort_num()[1]).replace('&', 'and'))

        if section is None:
            url = '/%s/' % tab_slug
        else:
            url = '/%s/%s/' % (tab_slug, section_slug)

        if self.type_id in (TYPE_ID_SUBSECTION,
                           TYPE_ID_PAGE,
                           TYPE_ID_TOPIC):
            url = '%s%d/' % (url, self.item_id)
        return url

    def strip_js(self):
        """Sanitise the HTML in summary, content and sub_content_b.

        Changes are not saved.

        """
        def _clean(html):
            return bleach.clean(
                html, tags=config['ALLOWED_HTML_TAGS'],
                attributes=config['ALLOWED_HTML_ATTRIBUTES'],
                styles=config['ALLOWED_HTML_STYLES'])

        self.summary = _clean(self.summary)
        self.content = _clean(self.content)
        self.sub_content_b = _clean(self.sub_content_b)

    def get_template(self):
        """ Hook to load custom templates. """

        # TODO: Hard-coding templates makes it difficult to re-use this
        # code. We need to push this configuration out to settings,
        # database, templates, or styles.

        (tab, _, _, _) = self.get_ancestors()
        if tab.slug() == 'blink':
            tab_template = "blink/base.html"
        else:
            tab_template = "vanilla/base.html"
        section_template = 'layouts/bicycle_network_page.html'

        return (tab_template, section_template)

    def extract_sort_num(self, pending=False):
        """Determine the index number and un-indexed title of the given page.

        :param pending: ``True`` → act on the page's “pending” content.
        :return: Two-item tuple of the index number and the remaining title
            text. If the page title has no index number, the index item is
            ``None``.

        """
        orig_title = self.title_pending if pending else self.title
        match = re.match("(\d+)\. (.+)$", orig_title)
        if match:
            index, title = match.groups()
            return (int(index), title)
        else:
            return (None, orig_title)

    def summary_text(self):
        return self.summary.strip() if self.summary is not None else ''

    def slug(self):
        return slugify((self.extract_sort_num()[1]).replace('&', 'and'))

    def use_home_layout(self):
        if self.slug() == 'home':
            return True
        return False

    def which_template(self):
        """Determine the Django template for displaying this item.

        This may eventually be extended to allow Items to specify a custom
        template.

        """
        if self.type_id in (TYPE_ID_SUBSECTION, TYPE_ID_TOPIC, TYPE_ID_PAGE):
            if self.show_sub_content_b:
                return 'vanilla/layout_menu_content_panel.html'
            else:
                return 'vanilla/layout_menu_content.html'

        else:
            if self.show_sub_content_b:
                return 'vanilla/layout_content_panel.html'
            else:
                return 'vanilla/layout_content.html'

    def get_level(self):
        return ItemType.objects.get(pk=self.type_id).title

    def status_text(self):
        """Returns a text description of the page status.

        This can be used for displaying alongside the title to provide
        additional information to authors.

        """
        status = ""
        self.query_lock()
        if self.has_lock():
            status = "in use"
        elif self.archived:
            status = "archived"
        elif (self.edit_mode_id == EDIT_MODE_READY
              and self.status_id == STATUS_ID_PENDING):
            status = "for approval"
        elif (self.edit_mode_id == EDIT_MODE_READY
              and self.status_id == STATUS_ID_NEW):
            status = "new for approval"
        elif (self.edit_mode_id == EDIT_MODE_DRAFT
              and self.status_id == STATUS_ID_PENDING):
            status = "draft"
        elif (self.edit_mode_id == EDIT_MODE_DRAFT
              and self.status_id == STATUS_ID_NEW):
            status = "new"
        else:
            status = "live"
        return status

    def get_page_menus(self, staff_menu=False, session_key=None):
        """Return menus

        Also checks if menu to be generated for staff (locked/
        unpublished/pending) or bv or bwa members

        NOTE: maybe we should return a dictionary instead?
        """
        main_menu = self.main_menu(staff_menu, session_key)
        sub_menu = self.sub_menu(staff_menu, session_key)
        this_section_menu = self.this_section_menu(staff_menu, session_key)
        page_menu = self.page_menu(staff_menu, session_key)

        return (main_menu, sub_menu, this_section_menu, page_menu)

    def menu(self, child_of_item, type_id, current, staff_menu=False, session_key=None):
        """Generate menu array for this item.

        Handles locked/unpublished/pending pages.
        """
        menu = []
        dont_show_items = []
        if staff_menu:
            status_id_list = [STATUS_ID_LIVE, STATUS_ID_PENDING, STATUS_ID_NEW]
        else:
            status_id_list = [STATUS_ID_LIVE, STATUS_ID_PENDING]
        items = Item.objects.filter(
            type_id=type_id,
            relate=child_of_item,
            archived=False,
            status_id__in=status_id_list,
            menu_hide=False).exclude(item_id__in=dont_show_items).order_by('title')
        items = items.filter(
            Q(publish_date__isnull=True) | Q(publish_date__lte=datetime.now()))

        for item in items:
            if current and item.item_id == current.item_id:
                classes = 'current'
            else:
                classes = ''

            title = item.extract_sort_num()[1]
            if staff_menu:
                title = item.extract_sort_num(pending=True)[1]
                if item.status_text() != 'live':
                    title += " ({0})".format(item.status_text())

            item.classes = classes
            # If we set item.title, it will break slugs
            item.display_title = title
            menu.append(item)
        return menu

    def main_menu(self, staff_menu=False, session_key=None):
        # BUG: session_key isn't being passed on, but it's mentioned
        # in menu() above.
        (tab, section, _, _) = self.get_ancestors()
        return self.menu(child_of_item=tab, type_id=TYPE_ID_SECTION,
                         current=section, staff_menu=staff_menu)

    def sub_menu(self, staff_menu=False, session_key=None):
        (_, section, subsection, _) = self.get_ancestors()
        return self.menu(child_of_item=section, type_id=TYPE_ID_SUBSECTION,
                         current=subsection, staff_menu=staff_menu)

    def this_section_menu(self, staff_menu=False, session_key=None):
        (_, section, subsection, topic) = self.get_ancestors()
        # for blink pages without subsections and still have topic
        if subsection is None:
            subsection = section
        return self.menu(child_of_item=subsection, type_id=TYPE_ID_TOPIC,
                         current=topic, staff_menu=staff_menu)

    def page_menu(self, staff_menu=False, session_key=None):
        """ build page menu
        """
        (_, _, _, topic) = self.get_ancestors()
        return self.menu(child_of_item=topic, type_id=TYPE_ID_PAGE,
                         current=self, staff_menu=staff_menu)

    def publish(self, commit=True):
        """Make draft version live."""
        self.status_id = STATUS_ID_LIVE
        self.archived = False
        self.title = self.title_pending
        self.summary = self.summary_pending
        self.content = self.content_pending
        self.sub_content_b = self.sub_content_b_pending
        self.show_sub_content_b = self.show_sub_content_b_pending
        if commit:
            self.save()

    def revert(self):
        """Remove draft version.

        Although it shouldn't be strictly necessary to set the content
        fields back to be the same a live, we'll do it anyway.
        """
        self.status_id = STATUS_ID_LIVE
        self.title_pending = self.title
        self.summary_pending = self.summary
        self.content_pending = self.content
        self.sub_content_b_pending = self.sub_content_b
        self.show_sub_content_b_pending = self.show_sub_content_b
        self.save()

    def archive(self, commit=False):
        """Archives this page (but not children)."""
        self.archived = True
        self.save()

    def unarchive(self):
        """Un-archives this page (but not children or parents).

        Be aware that non-archived page will only show in the menus if its
        parents are not archived.
        """
        self.archived = False
        self.save()

    def is_latest(self):
        """ Is this item already published in its latest revision?

            :return: False if the item has changes waiting to be
                published, otherwise True.

            """
        result = self.status_id == STATUS_ID_LIVE
        return result

    def is_published(self):
        """Should this page be shown to the public?"""
        result = (self.status_id in set([STATUS_ID_LIVE, STATUS_ID_PENDING]))
        if self.archived:
            result = False
        return result

    def add_change_log(self, user, change_entry, addedit, status):
        """Append to log entry to item.change_summary and also send entry to logger"""
        currtime = datetime.now()
        displaytime = currtime.strftime("%d %B %Y at %H:%M")
        end_line = '\n'
        self.change_summary_count += 1
        if change_entry != '':
            change_entry = end_line + 'Change summary: ' + change_entry
        new_entry = '{num}. {addedit} on {displaytime} by {username}{end_line}Page status: {status}{change_entry}{end_line}{end_line}'.format(
                        username=user.username, displaytime=displaytime, num=self.change_summary_count,
                        change_entry=change_entry, addedit=addedit,
                        status=status, end_line=end_line)
        self.change_summary = new_entry + self.change_summary
        logger.info(
            'User {user} {addedit} "{title}" ({item_id}) as {status}.'.format(
                user=user, item_id=self.item_id,
                title=(self.title or self.title_pending),
                status=status, addedit=addedit))
        self.save()
        return True


    def can_edit(self, user, session_key=None):
        """Is the user authorised to edit this page?"""
        if session_key:
            # No editing if page is locked.
            self.query_lock(session_key)
            if self.has_lock() and not self.is_locked():
                return False

        return user.has_perm('vanilla.add_edit_item')


    def can_add_below(self, user):
        """Is the user authorised to add child pages below this page?"""
        curr_level = self.get_level()

        # Can't add at botom level.
        if curr_level == 'Page':
            return False

        # Only superuser can add at Tab level (ie. add new Sections).
        if curr_level == 'Tab' and not user.is_superuser:
            return False

        # No-one can add below home page.
        #
        # TODO: Remove this special case by making home a Tab. To do this we'll
        # need a separate "slug" field though, so it can be called "Home" but
        # with slug "general".
        if self.slug() == 'home':
            return False

        return user.has_perm('vanilla.add_edit_item')

    def can_publish(self, user):
        """Is the user authorised to publish this page?"""
        # Very simple, but here for consistency.
        return user.has_perm('vanilla.publish_item')

    def can_archive(self, user):
        """Is the user authorised to archive this page?"""
        curr_level = self.get_level()

        # Only superuser can archive at Tab level.
        if curr_level in ['Tab', 'Section'] and not user.is_superuser:
            return False

        return user.has_perm('vanilla.publish_item')

    def has_archived_ancestors(self):
        """Returns a boolean advising whether a parent page is archived.

        It's inefficient because get_ancestors() is inefficient.

        """
        return any(
            [item for item in self.get_ancestors() if item and item.archived])

    class PromoteDemoteError(Exception):
        pass

    def make_child_of(self, new_parent):
        """Move this item to be a child of new_parent.

        This process also involves updating the Item.type of children if
        necessary.

        Raises a PromoteDemoteError if you attempt to demote an item (including
        any of its children) below the level of "page". Archived pages ending up
        below the "page" level will be silently deleted.

        """
        def demote_promote(item, type_change):
            """Recursively demote/promote this item and children.

            Args:
                item: the root item to be promoted/demoted
                type_change: integer representing relative change to type eg. -1

            """
            logger.debug('Demote/promote %s: %s', item, type_change)
            final_type_id = item.type_id + type_change
            if final_type_id < TYPE_ID_TAB or final_type_id > TYPE_ID_PAGE:
                if item.archive:
                    logger.debug("Archived item %s won't fit type range, deleting.", item)
                    item.delete()
                    return
                raise Item.PromoteDemoteError(
                    'Failed to change {} to {}.'.format(item, final_type_id))
            item.type_id = final_type_id
            item.save()
            children = Item.objects.filter(relate=item)
            logger.debug('Moving on to demote/promote children of %s: %s.', item, children)
            for child in children:
                demote_promote(child, type_change)

        # Will this page end up at the same level of hierarchy, or will it need
        # to be promoted or demoted?
        type_change = new_parent.type_id - self.relate.type_id

        with transaction.atomic():
            if type_change != 0:
                demote_promote(self, type_change)

            self.relate = new_parent
            self.save()

    def is_news(self):
        """Is this a news item?"""
        return self.relate_id == settings.VANILLA_CONFIG['NEWS_PARENT_ITEM']


pre_save.connect(hide_menu_in_news, sender=Item)


class ItemEditMode(models.Model):
    """
    TODO: Ben suggests removing this class and the associated database table.
    """
    item_edit_mode_id = models.IntegerField(primary_key=True, db_column='ItemEditModeId')
    title = models.CharField(max_length=100, db_column='ItemEditModeTitle', blank=True)
    class Meta:
        db_table = 'itemeditmode'


class ItemStatus(models.Model):
    """
    TODO: Ben suggests removing this class and the associated database
    table. It seems to only be used by inteface/tools_page_list.php.
    """
    item_status_id = models.IntegerField(primary_key=True, db_column='ItemStatusId')
    title = models.CharField(max_length=100, db_column='ItemStatusTitle', blank=True)
    class Meta:
        db_table = 'itemstatus'


class ItemType(models.Model):
    item_type_id = models.IntegerField(primary_key=True, db_column='ItemTypeId')
    title = models.CharField(max_length=255, db_column='ItemTypeTitle', blank=True)

    class Meta:
        db_table = 'itemtype'

    def __str__(self):
        return self.title


def fit_image_field(image_field, width, height):
    """Crop/resize the Django ImageFieldFile to fit the desired size."""

    image_field.open('rb')
    original = Image.open(image_field)
    modified = ImageOps.fit(
        original, (width, height), method=Image.ANTIALIAS)
    image_field.close()
    image_field.open('wb')
    modified.save(image_field, quality=90)
    image_field.close()


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
