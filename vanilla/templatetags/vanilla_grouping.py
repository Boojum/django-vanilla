from django import template

register = template.Library()

@register.filter
def groups_of(seq, n):
    """Returns an iterator of tuples length n from sequence.

    Final tuple will have remaining items, at least one, at most n.

    """
    seq = iter(seq)
    while seq:
        group = []
        for _ in range(n):
            try:
                group.append(next(seq))
            except StopIteration:
                if group:
                    yield group
                return
        yield group
