===========
Vanilla CMS
===========

**Please note**: Django Vanilla is no longer actively maintained.

``vanilla`` is a Django app for adding editable pages to your website. Editing is
intuitive. You simply browse the website to the relevant page and select "Edit".
Further, the editor interface is designed to look similar to the displayed
pages. In our experience, new staff members find this approach much easier than
having to navigate a separate Admin interface.

Quickstart
-----------

The app is straightforward to add to an existing Django . Firstly, update your
``INSTALLED_APPS`` setting:

.. code-block:: python

    INSTALLED_APPS = [
        ...
        'vanilla',
        ...
    ]

Secondly include Vanilla in your project URLconf. This should be the last rule
so that Vanilla can catch any URLs that aren't handled by other components:

.. code-block:: python

   urlpatterns = [
       ...
       url(r'^', include('vanilla.urls')),
   ] 


To run the tests:

.. code-block:: bash

    $ tox
