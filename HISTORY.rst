Release History
---------------

0.7.18 (2021-09-18)
+++++++++++++++++++

 - Note no longer actively maintained.


0.7.17 (2017-02-08)
+++++++++++++++++++

**Improvements**
 - Add GitLab continuous integration script to run pylint
 - Add ``seo_title`` and ``seo_description`` fields to admin


0.7.0 (2016-07-18)
++++++++++++++++++

**Improvements**

 - 0.7.12 Find location - IPware used to get ip function, working live after problems
 - 0.7.8 Find location - .dat file DB in setup.py for installation
 - 0.7.7 Find location - Increase logging level to info
 - 0.7.6 Find location - Remove hard coded IP
 - 0.7.5 Find location - Change loading of DB to new location
 - 0.7.0 Find location - Add try-catch for opening ip DB, move DB into vanilla


0.6.0 (2016-07-18)
++++++++++++++++++

**Improvements**

 - Find Location added using pygeoip and associated .dat locations file


0.5.0 (2016-06-30)
++++++++++++++++++

**Improvements**

 - 0.5.5 Fixes for subsite for new dynamic menu
 - 0.5.0 Expand/collapse js drop down sub-section menu added to top menu


0.4.0 (2016-06-06)
++++++++++++++++++

**Removed**

 - Remove underused Parker (intranet) and Blink (registered user) sections.


0.3.5 (2015-09-04)
++++++++++++++++++

**Improvements**

 - Default ``Item.edit_mode`` to draft.


0.3.4 (2015-08-05)
++++++++++++++++++

**Improvements**

 - Rename package to ``vanilla``.
 - Extend ``README.rst``.
